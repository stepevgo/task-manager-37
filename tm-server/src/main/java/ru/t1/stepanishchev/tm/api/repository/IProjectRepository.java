package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}