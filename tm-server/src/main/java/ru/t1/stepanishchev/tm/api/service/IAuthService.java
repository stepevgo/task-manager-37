package ru.t1.stepanishchev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Session;
import ru.t1.stepanishchev.tm.model.User;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void logout(@Nullable Session session);

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}