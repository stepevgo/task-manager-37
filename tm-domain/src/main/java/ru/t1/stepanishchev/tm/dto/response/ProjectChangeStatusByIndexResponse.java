package ru.t1.stepanishchev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}