package ru.t1.stepanishchev.tm.exception.user;

import ru.t1.stepanishchev.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect.");
    }

}